import React from 'react';
import './App.css';
import ScalerComp from './ScalerComp';
import TransactionList from './TransactionList';
import SumComp from './SumComp';
import BiggestValue from './BiggestValue';
import AppHeader from './AppHeader';
import AppFooter from './AppFooter';

function App() {
  return (
    <div className="App">
      <AppHeader />
      <ScalerComp />
      <TransactionList />
      <SumComp />
      <BiggestValue />
      <AppFooter />
    </div>
  );
}

export default App;
