import React from 'react';

const AppHeader = () => {
    return(
        <header className="App-header">
            <h1 className="App-header-h1">Transakcje walutowe</h1>
        </header>
    );
}

export default AppHeader;