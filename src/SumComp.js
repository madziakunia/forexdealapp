import React from 'react'
import { connect } from 'react-redux';

const SumComp = (props) => {
    return (
        <section className="section">
            <p className="middleHeader">Podsumowanie wszystkich transakcji: </p>
            <p className="SumComp-ps">Ilość transakcji: {props.sumlist.sindex}</p>
            <p className="SumComp-ps">Suma w euro: {props.sumlist.seuro.toFixed(2)}</p>
            <p className="SumComp-ps">Suma w złotówkach: {props.sumlist.spln.toFixed(2)}</p>
        </section>
    )
}

const mapStateToProps = (state) => ({
    sumlist: state.sumlist,
});

export default connect(mapStateToProps, {})(SumComp);