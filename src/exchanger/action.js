import types from './types';

const add = (key, name, euro, pln) => ({
    type: types.ADD_TRANS,
    key, 
    name,
    euro,
    pln
  });

  const update = (newlist) => {
    return{
      type: types.UPDATE_TRANS,
      newlist
  }};

  const change = (scaler) => ({
      type: types.CHANGE_SCALER,
      scaler
  });

  const removeElement = (newlist2) => ({
    type: types.REMOVE_ELEMENT_TRANS,
    newlist2
  });

  const sum = () => ({
    type: types.SUM_ALL_TRANS
  });

  const valuable = () => ({
    type: types.MOST_VALUABLE_TRANS
  }); 
  // dodaj dla wszyskich akcje

  export default {
      add,
      change,
      update,
      removeElement,
      sum,
      valuable //, more
  }