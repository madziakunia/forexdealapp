import types from './types';

const listTrans = {
    scaler: 4.3115,
    list: [],
    sumlist: {
      seuro: 0,
      spln: 0,
      sindex: 0
    },
    mostval: {
      key: 0,
      name: '',
      euro: 0,
      pln: 0
    }
}
  
const transitionReducer = (state = listTrans, action) => {
  switch(action.type){
    case types.ADD_TRANS:
      return {
        ...state, list: [...state.list, { key:action.key, name: action.name, euro: action.euro, pln: action.pln}]
      }
    case types.UPDATE_TRANS:
      return {
        ...state, list: action.newlist
      } 
    case types.CHANGE_SCALER: 
      return {
        ...state, scaler: action.scaler
      }
    case types.REMOVE_ELEMENT_TRANS:
      return {
        ...state, list: action.newlist2
      }
    case types.REMOVE_ALL_TRANS:
      return {
        ...state, list: []
      }
    case types.SUM_ALL_TRANS:
      let totaleuro = 0;
      let totalpln = 0;
      let totalindex = 0;

      for (let i of state.list) {
        totaleuro = totaleuro + i.euro;
        totalpln = totalpln + i.pln;
      }
      totalindex = state.list.length;

      return {
      ...state, sumlist: {seuro: totaleuro, spln: totalpln ,sindex: totalindex}
    } 
    case types.MOST_VALUABLE_TRANS: 
      if(state.list.length === 0) {
        return state;
      }
      let mostvalue = state.list.slice(0);
      mostvalue.sort((a,b) => (a.euro > b.euro) ? 1 : -1)
      //zmien nazwe biggesttrans.. 
      //obsłuż to a.euro === b.euro i wpisz to do tablicy, żeby wszystko wypisac? costakiego
      let len = mostvalue.length
      //console.log(mostvalue[len-1])

      return {
        ...state, mostval: mostvalue[len-1]
      }

    default: 
    return state;
  }
}

export default transitionReducer;