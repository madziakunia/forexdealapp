import React from 'react';
import { connect } from 'react-redux';
import { actionsTrans } from './exchanger';

const ScalerComp = (props) =>{

    const changeRef = React.createRef()

    const scalerChanger = (event) => {
        event.preventDefault();
        props.change(changeRef.current.value);

        let updatelist = []
        for(let i of props.list){
            updatelist.push({key: i.key, name: i.name,euro: i.euro, pln: (i.euro*changeRef.current.value)});
        }
        props.update(updatelist);
        props.sum();
        props.valuable();
    }

    return (
        <section className="section">
            <p className="ScalerComp-scaler">Obecny kurs walut:</p>
            <p>1 EURO = {props.scaler} PLN</p>
            <form onSubmit={scalerChanger}>
                <input className="input" type="number" step="0.0001" min="0" max="10000" ref={changeRef} placeholder="Wartość 1 euro"/>
                <button className="button" type="submit">Zmień przelicznik</button>
            </form>
        </section>
    )
}

const mapStateToProps = state => ({
    scaler: state.scaler,
    list: state.list
});

const mapDispatchToProps = dispatch => ({
    change: factor => dispatch(actionsTrans.change(factor)),
    update: newlist => dispatch(actionsTrans.update(newlist)),
    sum: () => dispatch(actionsTrans.sum()),
    valuable: () => dispatch(actionsTrans.valuable())
});

export default connect(mapStateToProps, mapDispatchToProps)(ScalerComp);