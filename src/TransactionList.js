import React from 'react';
import { connect } from 'react-redux';
import { actionsTrans } from './exchanger';

const TransactionList = (props) => {

    const transacRef = React.createRef()
    const euroRef = React.createRef()


    const handlerEvent = (event) => {
        event.preventDefault();
        // funkcja makeid() jest poglądowa w celu dodania key przy mapowaniu elementów
        // przy połaczeniu z backendem id byłoby generowane w bazie danych 
        const makeid = () =>  {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          
            for (var i = 0; i < 5; i++)
              text += possible.charAt(Math.floor(Math.random() * possible.length));
          
            return text;
          }
        props.add(makeid(), transacRef.current.value, parseInt(euroRef.current.value, 10), (euroRef.current.value*props.scaler));
        props.sum();
        props.valuable();
        euroRef.current.value = '';
        transacRef.current.value = '';
    }

    const handlerRemove = (e) => {
        let newlist2 = props.lists.slice(0);
        for(let i of props.lists) {
            if(i.key === e.target.parentElement.id){
                newlist2.splice(props.lists.indexOf(i), 1);
                props.removeElem(newlist2);
                props.sum();
                props.valuable();
            }
        }
    }

    return (
        <>
            <section className="section">
                <p className="middleHeader">Lista transakcji: </p>
                <p style={{display: props.lists.length > 0 ? "none" : "block"}}>Brak zawartych transakcji.</p>
                <table style={{display: props.lists.length > 0 ? "table" : "none"}}>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Euro</th>
                            <th>Pln</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.lists.map( ob => 
                        <tr id={ob.key} key={ob.key}>
                            <td>{ob.name}</td>
                            <td>{ob.euro.toFixed(2)}</td>
                            <td>{ob.pln.toFixed(2)}</td>
                            <td className="TransactionList-usun" onClick={handlerRemove}>Usuń</td>
                        </tr>)}
                    </tbody>
                </table>
            </section>

            <section className="section">   
                <p className="middleHeader">Dodaj nową transakcje: </p>

                <form onSubmit={handlerEvent}>
                    <input  className="input"ref={transacRef} placeholder="Nazwa transakcji" required={true}/>
                    <input type="number" step="0.01" min="0" max="100000000" className="input" ref={euroRef} placeholder="Wartość w euro"/>
                    <button className="button" type="submit">Dodaj transakcje</button>
                </form>
            </section>
        </>
    )
}

const mapStateToProps = state => ({
    lists: state.list,
    scaler: state.scaler
});

const mapDispatchToProps = dispatch => ({
    add: (key, name, euro, pln) => dispatch(actionsTrans.add(key, name, euro, pln)),
    removeElem: (number) => dispatch(actionsTrans.removeElement(number)),
    sum: () => dispatch(actionsTrans.sum()),
    valuable: () => dispatch(actionsTrans.valuable())
});

export default connect(mapStateToProps, mapDispatchToProps)(TransactionList);

/*



*/