import React from 'react';
import { connect } from 'react-redux';

const BiggestValue = (props) => {
    return(
        <section className="section">
            <p className="middleHeader">Transakcja o najwyższej wartości: </p>
            <p className="MostValue-p">name: {props.valuea.name} </p>
            <p className="MostValue-p">euro: {props.valuea.euro.toFixed(2)} </p>
            <p className="MostValue-p">pln:  {props.valuea.pln.toFixed(2)}</p>
        </section>
    );
}

const mapStateToProps = state => ({
    valuea: state.mostval
});


export default connect(mapStateToProps, {})(BiggestValue)