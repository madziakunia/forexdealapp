import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { transitionReducer } from './exchanger';

const store = createStore(
    transitionReducer,
    composeWithDevTools()
);


export default store;